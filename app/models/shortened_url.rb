require 'securerandom'
class ShortenedUrl < ActiveRecord::Base
  attr_accessible :short_url, :long_url, :submitter_id
  validates :short_url, :uniqueness => true, :presence => true
  validates :long_url, :presence => true
  validates :submitter_id, :presence => true

  def self.random_code
    SecureRandom::urlsafe_base64
  end

  def self.create_for_user_and_long_url!(user, long_url)
    short_url = self.random_code
    ShortenedUrl.new(:short_url => short_url,
                    :long_url => long_url, :submitter_id =>user.id)
  end

  def num_clicks
    self.visits.count
  end

  def num_uniques
    user_ids = []
    self.visits.each do |visit|
      user_ids << visit.user_id
    end

    user_ids.uniq.count
  end

  belongs_to(
    :submitter,
    :class_name => 'User',
    :foreign_key => :submitter_id,
    :primary_key => :id
    )

    has_many(
      :visits,
      :class_name => 'Visit',
      :foreign_key => :shortened_url_id,
      :primary_key => :id
  )

  has_many :visitors, :through => :visits, :source => :visitor


end